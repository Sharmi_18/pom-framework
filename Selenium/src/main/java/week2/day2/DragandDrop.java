package week2.day2;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.interactions.Actions;
public class DragandDrop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/Chromedriver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
	driver.get("http://jqueryui.com/draggable/");
		
driver.manage().window().maximize();
driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
driver.switchTo().frame(0);
WebElement dragabble = driver.findElementById("draggable");
Actions builder=new Actions(driver);
builder.dragAndDropBy(dragabble, 50, 40).perform();

	}

}
