package week2.day2;

import org.omg.Messaging.SyncScopeHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Createlead {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();

		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DEMOCSR");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByXPath("(//a[text()='Create Lead'])[1]").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("CTS");
		driver.findElementById("createLeadForm_firstName").sendKeys("Sharmi");
		driver.findElementById("createLeadForm_lastName").sendKeys("Askar");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Sharmila");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("BA");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Mrs");
		WebElement dropdown1 = driver.findElementById("createLeadForm_dataSourceId");
		Select dd1=new Select(dropdown1);
		dd1.selectByIndex(3);
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Mrs");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("150000");
		WebElement dropdown2 = driver.findElementById("createLeadForm_industryEnumId");
		Select dd2=new Select(dropdown2);
		dd2.selectByIndex(5);
		WebElement dropdown3 = driver.findElementById("createLeadForm_ownershipEnumId");
		Select dd3=new Select(dropdown3);
		dd3.selectByValue("OWN_LLC_LLP");
		driver.findElementById("createLeadForm_sicCode").sendKeys("123");
		driver.findElementById("createLeadForm_description").sendKeys("testing ");
		driver.findElementById("createLeadForm_importantNote").sendKeys("darl");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("2");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("+91");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("9940");
		driver.findElementById("createLeadForm_departmentName").sendKeys("IT");
		WebElement dropdown4 = driver.findElementById("createLeadForm_currencyUomId");
		Select dd4=new Select(dropdown4);
		dd4.selectByIndex(6);
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("5");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("love");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("lala");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("http://tiny.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("test");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("123 street");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("selaiyur");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600047");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("044");
		WebElement dropdown5 = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select dd5=new Select(dropdown5);
		dd5.selectByVisibleText("Indiana");
		WebElement dropdown6 = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select dd6=new Select(dropdown6);
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9940263803");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("Sharmila.it18@gmail.com");
		driver.findElementByXPath(" //input[@value='Create Lead']").click();
		//System.out.println(driver.findElementById("createLeadForm_firstNameLocal").getAttribute("value"));
		driver.close();
	}



}
