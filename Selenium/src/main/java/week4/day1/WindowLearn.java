package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowLearn {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();
		driver.findElementByXPath("(//span[text()='AGENT LOGIN'])[1]").click();
		driver.findElementByLinkText("Contact Us").click();
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> lsref=new ArrayList<String>();
		lsref.addAll(windowHandles);
		WebDriver window1 = driver.switchTo().window(lsref.get(0));
		//window1.close();
		System.out.println(window1.getTitle());
		File source = driver.getScreenshotAs(OutputType.FILE);
		File dest= new File("./snaps/image2.png");
		FileUtils.copyFileToDirectory(source, dest);
		WebDriver window2 = driver.switchTo().window(lsref.get(1));
		//driver.switchTo().window(lsref.get(0)).close();
		
		System.out.println(window2.getTitle());
		
		File source1 = driver.getScreenshotAs(OutputType.FILE);
		File dest1= new File("./snaps/image.png");
		FileUtils.copyFileToDirectory(source1, dest1);
		
	}

}
