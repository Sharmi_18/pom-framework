package week4.day2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ZoomcarMaxamont {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");

		ChromeOptions ops = new ChromeOptions();
		ops.addArguments("--disable-notifications");
		ChromeDriver driver=new ChromeDriver(ops);
		
driver.get("https://www.zoomcar.com/chennai/");
driver.manage().window().maximize();
driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
driver.findElementByLinkText("Start your wonderful journey").click();
Thread.sleep(3000);
driver.findElementByXPath("//div[@class='component-popular-locations']/div[5]").click();
driver.findElementByClassName("proceed").click();
driver.findElementByXPath("//div[@class='days']/div[2]").click();
driver.findElementByXPath("//button[text()='Next']").click();
driver.findElementByXPath("//button[text()='Done']").click();
List<WebElement> price = driver.findElementsByXPath("//div[@class='price']");
List <Integer> list=new ArrayList<Integer>();
for(int i=0;i<price.size();i++)
{
	String text = price.get(i).getText();
	String replaceAll = text.replaceAll("\\D", "");
	int result=Integer.parseInt(replaceAll);
	list.add(result);
}
int max = Collections.max(list);
System.out.println(max);
String string = driver.findElementByXPath("//div[contains(text(),'"+max+"')]/preceding::h3[1]").getText();
System.out.println(string);
driver.close();   
	}

}
