package week4.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Mergelead {
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		ChromeOptions ops = new ChromeOptions();
		ops.addArguments("--disable-notifications");
		ChromeDriver driver=new ChromeDriver(ops);
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DEMOSALESMANAGER");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByXPath("(//a[text()='Create Lead'])[1]").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("//img[@alt='Lookup'][1]").click();
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> list=new ArrayList<String>();
		list.addAll(windowHandles);
		WebDriver window1 = driver.switchTo().window(list.get(1));
		System.out.println(window1.getTitle());
		window1.findElement(By.name("id")).sendKeys("10020");
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(window1.findElement(By.xpath("(//button[@class='x-btn-text'])[1]")))).click();
		Thread.sleep(5000);
		window1.findElement(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")).click();
		WebDriver window2 = driver.switchTo().window(list.get(0));
		System.out.println(window2.getTitle());

		window2.findElement(By.xpath("(//img[@alt='Lookup'])[2]")).click();
		Set<String> windowHandles2 = driver.getWindowHandles();
		List<String> list2=new ArrayList<String>();
		list2.addAll(windowHandles2);

		WebDriver window3 = driver.switchTo().window(list2.get(1));
		System.out.println(window3.getTitle());
		window3.findElement(By.name("id")).sendKeys("10021");
		WebDriverWait wait1 = new WebDriverWait(driver, 20);
		wait1.until(ExpectedConditions.visibilityOf(window3.findElement(By.xpath("(//button[@class='x-btn-text'])[1]")))).click();
		Thread.sleep(5000);
		window3.findElement(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")).click();
		WebDriver window4 = driver.switchTo().window(list.get(0));
		System.out.println(window4.getTitle());
		driver.findElementByLinkText("Merge").click();
		Alert alert = driver.switchTo().alert();
		alert.accept();
		driver.close();



	}

}
