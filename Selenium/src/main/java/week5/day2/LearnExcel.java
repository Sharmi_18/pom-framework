package week5.day2;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcel {

	public static String[][] readExcel(String Filename) throws InvalidFormatException, IOException {
		// TODO Auto-generated method stub
XSSFWorkbook workbook=new XSSFWorkbook (new File("./data/"+Filename+".xlsx"));
XSSFSheet sheet = workbook.getSheetAt(0);
int lastRowNum = sheet.getLastRowNum();
System.out.println(lastRowNum);
int cells = sheet.getRow(0).getLastCellNum();
System.out.println(cells);
String[][] data=new String[lastRowNum][cells];
for(int i=1;i<=lastRowNum;i++)
{
	XSSFRow row = sheet.getRow(i);
	for(int j=0;j<cells;j++)
	{
	XSSFCell cellt = row.getCell(j);
	data[i-1][j] = cellt.getStringCellValue();
	
	}
}
workbook.close();
return data;
	}

}
