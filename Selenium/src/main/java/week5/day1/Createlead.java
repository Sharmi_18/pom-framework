package week5.day1;

import org.omg.Messaging.SyncScopeHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Createlead extends LearnAnnotations {
	@Test(dataProvider="Fetchdata")
	public void CreateLeadT(String Company,String firstname,String lastname) {
		// TODO Auto-generated method stub
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByXPath("(//a[text()='Create Lead'])[1]").click();
		driver.findElementById("createLeadForm_companyName").sendKeys(Company);
		driver.findElementById("createLeadForm_firstName").sendKeys(firstname);
		driver.findElementById("createLeadForm_lastName").sendKeys(lastname);
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Sharmila");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("BA");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Mrs");
		WebElement dropdown1 = driver.findElementById("createLeadForm_dataSourceId");
		Select dd1=new Select(dropdown1);
		dd1.selectByIndex(3);
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Mrs");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("150000");
		WebElement dropdown2 = driver.findElementById("createLeadForm_industryEnumId");
		Select dd2=new Select(dropdown2);
		dd2.selectByIndex(5);
		WebElement dropdown3 = driver.findElementById("createLeadForm_ownershipEnumId");
		Select dd3=new Select(dropdown3);
		dd3.selectByValue("OWN_LLC_LLP");
		driver.findElementById("createLeadForm_sicCode").sendKeys("123");
		driver.findElementById("createLeadForm_description").sendKeys("testing ");
		driver.findElementById("createLeadForm_importantNote").sendKeys("darl");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("2");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("+91");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("9940");
		driver.findElementById("createLeadForm_departmentName").sendKeys("IT");
		WebElement dropdown4 = driver.findElementById("createLeadForm_currencyUomId");
		Select dd4=new Select(dropdown4);
		dd4.selectByIndex(6);
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("5");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("love");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("lala");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("http://tiny.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("test");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("123 street");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("selaiyur");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600047");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("044");
		WebElement dropdown5 = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select dd5=new Select(dropdown5);
		dd5.selectByVisibleText("Indiana");
		WebElement dropdown6 = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select dd6=new Select(dropdown6);
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9940263803");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("Sharmila.it18@gmail.com");
		driver.findElementByXPath(" //input[@value='Create Lead']").click();
		//System.out.println(driver.findElementById("createLeadForm_firstNameLocal").getAttribute("value"));
		
	}
@DataProvider(name="Fetchdata")
public String[][] input(){
	String[][] data=new String[2][3];
	data[0][0]="CTS";
	data[0][1]="Sharmi";
	data[0][2]="A";
	data[1][0]="CTSS";
	data[1][1]="Sharmila";
	data[1][2]="Askar";
	return data;
	
	
	
}

}
