package week6.day1;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class DuplicateLead extends LearnAnnotations {
	@Test
	public  void DuplicateLeadT() throws InterruptedException{
		// TODO Auto-generated method stub
		
		driver.findElementByLinkText("CRM/SFA").click();
		Thread.sleep(5000);
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByLinkText("Email").click();
		driver.findElementByName("emailAddress").sendKeys("Sharmila.it18@gmail.com");
		Thread.sleep(3000);
		driver.findElementByLinkText("Find Leads").click();
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]"))).click();
		driver.findElementByLinkText("Duplicate Lead").click();
		System.out.println(driver.getTitle());
		driver.findElementByName("submitButton").click();
		
	}

}


