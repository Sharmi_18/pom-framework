package week6.day1;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

public class LearnAnnotations {
	public String excelFileName;
	public  ChromeDriver driver;
@Parameters({"URL","username","password"})
@BeforeMethod
public void Login(String url,String username,String password) {
	System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
	driver=new ChromeDriver();
	driver.get(url);
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	driver.findElementById("username").sendKeys(username);
	driver.findElementById("password").sendKeys(password);
	driver.findElementByClassName("decorativeSubmit").click();
	
}
@AfterMethod
public void close() {
	driver.close();
}
@DataProvider(name="Fetchdata")
public String[][] createLeadData() throws InvalidFormatException, IOException
{
	return LearnExcel.readExcel(excelFileName);
}
}