package week3.Day2;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class MapExam {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String text="Amazon India Private Limited";
		char[] charArray = text.toCharArray();
		Map<Character,Integer> var= new LinkedHashMap<Character,Integer>();
		for (char c : charArray) {
			
			if(var.containsKey(c))
				var.put(c, var.get(c)+1);
			else
				var.put(c, 1);
		}
		System.out.println(var);

	
	
	}

}
