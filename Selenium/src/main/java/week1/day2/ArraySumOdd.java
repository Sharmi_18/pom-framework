package week1.day2;

public class ArraySumOdd {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int oddNumber[]= {3,10,13,25,40};
		int result=0;
		for (int i=0;i<oddNumber.length;i++)
		{

			if(oddNumber[i]%2!=0)
				result=result+oddNumber[i];

		}
		System.out.println("Sum of Odd numbers in array is "+result);
	}

}
