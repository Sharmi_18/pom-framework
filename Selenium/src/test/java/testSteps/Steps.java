package testSteps;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Steps {
	public static String firstlead;
	public static String companyname;
	public ChromeDriver driver;

	//TC001 create lead
	@Given("Open the Chrome browser")
	public void openTheChromeBrowser() {
		// Write code here that turns the phrase above into concrete actions
		System.setProperty("webdriver.chrome.driver","./drivers/Chromedriver/chromedriver.exe");
		driver=new ChromeDriver();
	}

	@Given("Maximize the browser")
	public void maximizeTheBrowser() {
		// Write code here that turns the phrase above into concrete actions
		driver.manage().window().maximize(); 
	}

	@Given("Set the timeout")
	public void setTheTimeout() {
		// Write code here that turns the phrase above into concrete actions
		driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
	}

	@Given("Load the URL")
	public void loadTheURL() {
		// Write code here that turns the phrase above into concrete actions
		driver.get("http://leaftaps.com/opentaps");
	}

	@Given("Enter the User name as (.*)")
	public void enterTheUserNameAsDemoSalesManager(String UName) {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementById("username").sendKeys(UName);
	}

	@Then("^Enter the Password \"(.*)\"$")
	public void enterThePasswordAsCrmsfa(String Password) {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementById("password").sendKeys(Password); 
	}

	@When("Click on Login Button")
	public void clickOnLoginButton() {

		// Write code here that turns the phrase above into concrete actions
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@When("Click on CRM\\/SFA button")
	public void clickOnCRMSFAButton() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@When("Click on Create Lead menu")
	public void clickOnCreateLeadMenu() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("(//a[text()='Create Lead'])[1]").click();   
	}
	@When("Enter FirstName as (.*)")
	public void enterFirstName(String FN) {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementById("createLeadForm_firstName").sendKeys(FN);   
	}

	@When("Enter Last name as (.*)")
	public void enterLastName(String LN) {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementById("createLeadForm_lastName").sendKeys(LN);
	}

	@When("Enter Company name as (.*)")
	public void enterCompanyName(String CN) {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementById("createLeadForm_companyName").sendKeys(CN);
	}

	@When("Click on submit button")
	public void clickOnSubmitButton() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath(" //input[@value='Create Lead']").click();  
	}

	@Then("The Lead should be created")
	public void theLeadShouldBeCreated() {
		// Write code here that turns the phrase above into concrete actions
		System.out.println("Pass");
		driver.close();
	}

	//TC002 -Delete Lead
	@Given("Click on Create Lead menu1")
	public void clickOnCreateLeadMenu1() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Leads").click();
	}

	@Given("Click on Find lead menu")
	public void clickOnFindLeadMenu() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Find Leads").click(); 
	}

	@Given("Click on Phone menu")
	public void clickOnPhoneMenu() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("//span[text()='Phone']").click(); 
	}

	@Given("Click on Enter phone number as (.*)")
	public void clickOnEnterPhoneNumberAsPhone(String ph) {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath(" //input[@name='phoneNumber']").sendKeys(ph);
	}

	@Given("Click find leads button")
	public void clickFindLeadsButton() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("//button[text()='Find Leads']").click();
	}

	/*@Given("Capture lead ID of First Resulting lead")
	public void captureLeadIDOfFirstResultingLead() {
	    // Write code here that turns the phrase above into concrete actions
	 driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]");
	//System.out.println(firstlead);
	}*/

	@Given("Click First Resulting lead")
	public void clickFirstResultingLead() throws InterruptedException {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(5000);
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]").click();
	}

	@When("Click Delete")
	public void clickDelete() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByClassName("subMenuButtonDangerous").click();
	}

	@Then("Verify the Lead is deleted")
	public void verifyTheLeadIsDeleted() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		driver.findElementByXPath("//input[@name='id']").sendKeys(firstlead);
		driver.findElementByLinkText("//button[text()='Find Leads']").click();
		String leadtext = driver.findElementByLinkText("No records to display").getText();
		System.out.println(leadtext);
		driver.close();
	}



	//TC004:Merge Lead


	@Given("Click on Leads link to merge")
	public void clickOnLeadsLinkToMerge() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("//a[text()='Leads']").click(); 
	}

	@Given("Click on Metge Lead menu")
	public void clickOnMetgeLeadMenu() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("//a[text()='Merge Leads']").click();
	}

	@Given("Click on Icon near From Lead")
	public void clickOnIconNearFromLead() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
	}

	@Given("Move to new window")
	public void moveToNewWindow() throws InterruptedException {
		// Write code here that turns the phrase throw new cucumber.api.PendingException();
		Set<String> windowHandles = driver.getWindowHandles();
		List <String> list=new ArrayList<String>();
		list.addAll(windowHandles);
		WebDriver window1 = driver.switchTo().window(list.get(1));
		System.out.println(window1.getTitle());
		Thread.sleep(1000);
	}

	@Given("Enter from Lead ID as (.*)")
	public void enterFromLeadID (String id){
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByName("id").sendKeys( id);
	}

	@Given("Click Find Leads button after entering Lead ID")
	public void clickFindLeadsButtonAfterEnteringLeadID() throws InterruptedException {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
	}

	@Given("Click First Resulting lead in the from table")
	public void clickFirstResultingLeadInTheFromTable() throws InterruptedException {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
		Thread.sleep(1000);
	}

	@Given("Switch back to parent window")
	public void switchBackToparentWindow() {
		// Write code here that turns the phrase above into concrete actions
		Set<String> windowHandles = driver.getWindowHandles();
		List <String> list=new ArrayList<String>();
		list.addAll(windowHandles);
		WebDriver window2 = driver.switchTo().window(list.get(0));
		System.out.println(window2.getTitle());
	}

	@Given("Click on Icon near To Lead")
	public void clickOnIconNearToLead() throws InterruptedException {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		Thread.sleep(1000);
	}

	@Given("Enter To Lead ID as(.*)")
	public void enterToLeadID(String id) throws InterruptedException {
		// Write code here that turns the phrase above into concrete actions
		Set<String> windowHandles1 = driver.getWindowHandles();
		List <String> list1=new ArrayList<String>();
		list1.addAll(windowHandles1);
		WebDriver window3 = driver.switchTo().window(list1.get(1));
		System.out.println(window3.getTitle());
		Thread.sleep(1000);
		driver.findElementByName("id").sendKeys(id);
	}

	@Given("Click Find Leads button after to lead")
	public void clickFindLeadsButtonAfterToLead() throws InterruptedException {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
	}

	@When("Click First Resulting lead from To table")
	public void clickFirstResultingLeadFromToTable() throws InterruptedException {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
		Thread.sleep(1000);
	}

	@When("Switch back to primary window")
	public void switchBackToPrimaryWindow() {
		// Write code here that turns the phrase above into concrete actions
		Set<String> windowHandles1 = driver.getWindowHandles();
		List <String> list1=new ArrayList<String>();
		list1.addAll(windowHandles1);
		WebDriver window4 = driver.switchTo().window(list1.get(0));
		System.out.println(window4.getTitle());
	}

	@Then("Click Merge button")
	public void clickMergeButton() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("//a[text()='Merge']").click();
	}

	@Then("Accept Alert")
	public void acceptAlert() {
		// Write code here that turns the phrase above into concrete actions
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	@Then("Click Find Leads after alert closed")
	public void clickFindLeadsAfterAlertClosed() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("//a[text()='Find Leads']").click();
	}

	@Then("Enter From Lead ID which is captured")
	public void enterFromLeadIDWhichIsCaptured() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("//input[@name='id']").sendKeys("12640");
	}

	@Then("Click Find Leads and Verify error msg")
	public void clickFindLeadsAndVerifyErrorMsg() throws InterruptedException {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		String text = driver.findElementByXPath("//div[text()='No records to display']").getText();
		System.out.println(text);
		if (text.equals("No records to display"))
			System.out.println("Merged sucess");
		else
			System.out.println("non merged");
		driver.close();
	}


	//TC003 Edit lead

	@Given("Click on Leads link")
	public void clickOnLeadsLink() {
		// Write code here that turns the phrase above into concrete actions

		driver.findElementByLinkText("Leads").click();
	}

	@Given("Click on find leads button")
	public void clickOnFindLeadsButton() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Find Leads").click();
	}

	@Given("Enter the first name as (.*)")
	public void enterTheFirstName(String FN) {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(FN);
	}

	@Given("Click on find leads button again")
	public void Clickfindleadsbuttonagain() throws InterruptedException {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
	}
	@Given("Click on first record")
	public void clickOnFirstRecord() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]").click(); 
	}

	@When("Verify the title")
	public void verifyTheTitle() {
		// Write code here that turns the phrase above into concrete actions
		String title = driver.getTitle();
		System.out.println(title);
	}

	@When("Click on Edit button")
	public void clickOnEditButton() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Edit").click();
	}

	@When("Change the company name")
	public void changeTheCompanyName() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("TCS");
		companyname= driver.findElementById("updateLeadForm_companyName").getAttribute("value");
	}

	@Then("click update button")
	public void clickUpdateButton() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("(//input[@class='smallSubmit'])[1]").click();

	}

	@Then("COnfirm the chnaged name appears")
	public void confirmTheChnagedNameAppears() {
		// Write code here that turns the phrase above into concrete actions
		driver.close();
	}

	//TC005 Duplicate Lead
	@When("Click Leads menu link to duplicate")
	public void clickLeadsMenuLinkToDuplicate() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Leads").click();
	}

	@When("Click on Find leads from menu")
	public void clickOnFindLeadsFromMenu() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Find Leads").click();
	}

	@When("Click on Email")
	public void clickOnEmail() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Email").click();
	}

	@When("Enter Email as (.*)")
	public void enterEmail(String email){
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByName("emailAddress").sendKeys(email);
	}

	@When("Click find leads button to get results")
	public void clickFindLeadsButtonToGetResults() throws InterruptedException {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(1000);
	}

	@When("Click First Resulting lead from results")
	public void clickFirstResultingLeadFromResults() {
		// Write code here that turns the phrase above into concrete actions
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(
				driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]"))).click();
	}

	@Then("Click Duplicate Lead")
	public void clickDuplicateLead() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Duplicate Lead").click();
	}

	@Then("Verify the title of page")
	public void verifyTheTitleOfPage() {
		// Write code here that turns the phrase above into concrete actions
		System.out.println(driver.getTitle());
	}

	@Then("Click Create Lead")
	public void clickCreateLead() {
		// Write code here that turns the phrase above into concrete actions
		driver.findElementByName("submitButton").click();
	}







}	

