package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features="src/test/java/feature/login.feature", glue="testSteps",monochrome=true)
//dryRun=true,snippets=SnippetType.CAMELCASE)
public class Runner extends AbstractTestNGCucumberTests{
	

}
