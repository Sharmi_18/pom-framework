Feature: To Create Lead in Leaftab app

Background:
Given Open the Chrome browser
And Maximize the browser
And Set the timeout
And Load the URL
And Enter the User name as DemoSalesManager
And Enter the Password "crmsfa" 
When Click on Login Button
And Click on CRM/SFA button


Scenario Outline: TC001 Create Lead positive

And Click on Create Lead menu
And Enter FirstName as <FN>
And Enter Last name as <LN>
And Enter Company name as <CompanyName>
And Click on submit button
Then The Lead should be created
Examples:
 |CompanyName|FN|LN|
 |MIT|Sharmi|S|

Scenario: TC002 Delete Lead postive

Given Click on Create Lead menu1
And Click on Find lead menu
And Click on Phone menu
And Click on Enter phone number as 9940263803
And Click find leads button
And Click First Resulting lead
When Click Delete
Then Verify the Lead is deleted

Scenario: TC003 Edit Lead 
Given Click on Leads link
And Click on find leads button
And Enter the first name as Sharmi
And Click on find leads button again 
And Click on first record
When Verify the title
And Click on Edit button
And Change the company name
Then click update button
And COnfirm the chnaged name appears

Scenario: TC004 Merge Lead 
Given Click on Leads link to merge
And Click on Metge Lead menu 
And Click on Icon near From Lead
And Move to new window 
And Enter from Lead ID as 10203
And Click Find Leads button after entering Lead ID
And Click First Resulting lead in the from table
And Switch back to primary window
And Click on Icon near To Lead
And Move to new window
And Enter To Lead ID as 10204 
And Click Find Leads button after to lead
When Click First Resulting lead from To table
And Switch back to parent window  
Then Click Merge button
And Accept Alert
And Click Find Leads after alert closed
And Enter From Lead ID which is captured
And Click Find Leads and Verify error msg

Scenario Outline: TC005 Duplicate Lead
And Click Leads menu link to duplicate
And Click on Find leads from menu
And Click on Email 
And Enter Email as <Email>
And Click find leads button to get results
And Click First Resulting lead from results
Then  Click Duplicate Lead
And Verify the title of page  
And Click Create Lead

Examples:
|Email|
|Sharmila.it18@gmail.com|

 